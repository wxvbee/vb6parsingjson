VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   18720
   LinkTopic       =   "Form1"
   ScaleHeight     =   7455
   ScaleWidth      =   18720
   StartUpPosition =   3  '窗口缺省
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4080
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ListBox List1 
      Height          =   6000
      Left            =   5520
      TabIndex        =   3
      Top             =   1200
      Width           =   12735
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   6015
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   10610
      _Version        =   393217
      TextRTF         =   $"Form1.frx":0000
   End
   Begin VB.CommandButton Command2 
      Caption         =   "解析"
      Height          =   495
      Left            =   2160
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "读取"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    CommonDialog1.DialogTitle = "选择json文件"
    CommonDialog1.Filter = "(JSON文件)*.json|*.json"
On Error GoTo CancelError:
    CommonDialog1.CancelError = True
    CommonDialog1.ShowOpen
    RichTextBox1.Text = Get_json(CommonDialog1.FileName)
   Exit Sub
CancelError:

End Sub

Private Sub Command2_Click()
    Dim jobj As Object
    Set jobj = JSONParse("data", RichTextBox1.Text)
    '    ''----------------
    Dim s As Object, i As Integer
    i = 0

    For Each s In jobj
    
        List1.AddItem (s.Id & "|" & s.AssetTypeName & "|" & s.Name & "|" & s.Specs & "|" & s.UseCompanyName & "|" & s.UseDepartmentName & "|" & s.UserEmployeeId)

    Next
    
End Sub



Public Function JSONParse(ByVal JSONPath As String, ByVal JSONString As String) As Object
    On Error GoTo ErrH
    Dim JSON As Object
    Set JSON = CreateObject("MSScriptControl.ScriptControl")
    JSON.Language = "JavaScript"
    JSON.AddCode "var Json = " & JSONString & ";"
    Set JSONParse = JSON.Eval("Json." & JSONPath)
    Set JSON = Nothing
    Exit Function
ErrH:
    Debug.Print Err.Description
    Err.Clear
End Function


Public Function Get_json(path As String) As String
    Dim oStream As Object
    Set oStream = CreateObject("Adodb.Stream")
    oStream.Open
    oStream.Charset = "UTF-8"
    oStream.LoadFromFile path
    Get_json = oStream.ReadText()
    oStream.Close
End Function



